<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/welcome', [App\Http\Controllers\HomeController::class, 'show_begin_page'])->name('welcome');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/localization', [App\Http\Controllers\HomeController::class, 'localization'])->name('localization');
Route::post('/localization/store', [App\Http\Controllers\HomeController::class, 'store_localization'])->name('store_localization');
//ubacila da stavim routes->name
Route::get('/institution', [App\Http\Controllers\InstitutionsController::class, 'index'])->name('institution');
Route::post('/institution/store', [App\Http\Controllers\InstitutionsController::class, 'store'])->name('store_institution');

Route::get('/settings/language', [App\Http\Controllers\LanguagesController::class, 'show'])->name('language');
Route::delete('/settings/{language}/delete', [App\Http\Controllers\LanguagesController::class, 'delete'])->name('delete_language');  
Route::post('/language/store', [App\Http\Controllers\LanguagesController::class, 'store'])->name('store_language');

Route::get('/settings/users', [App\Http\Controllers\HomeController::class, 'show'])->name('users');
// Route::delete('/settings/{delete_user}/deleteUser', [App\Http\Controllers\HomeController::class, 'deleteUser'])->name('delete_user');

// Route::get('/settings/{update_user}/updateUser', [App\Http\Controllers\HomeController::class, 'showEditBladeUser'])->name('show_edit_blade_user');  
// Route::post('/settings/{update_user}/updateUser', [App\Http\Controllers\HomeController::class, 'updateUser'])->name('update_user'); //pokazati za editanje

Route::get('/authors/showAuthors', [App\Http\Controllers\AuthorsController::class, 'showAuthors'])->name('authors');
Route::post('/authors/store', [App\Http\Controllers\AuthorsController::class, 'storeAuthor'])->name('store_author');
Route::get('/authors/addAuthor', [App\Http\Controllers\AuthorsController::class, 'showLanguages'])->name('show_language');

Route::get('/authors/editAuthor/{author_id}/{author_name}', [App\Http\Controllers\AuthorsController::class, 'showEditBladeAuthor'])->name('show_edit_blade_author');
Route::post('/authors/editAuthor/{author_id}', [App\Http\Controllers\AuthorsController::class, 'editAuthor'])->name('edit_author'); 

//Route::view('/authors/addBiography', 'authors.addBiography');
Route::post('/authors/storeBiography', [App\Http\Controllers\BiographiesController::class, 'storeBiography'])->name('store_biography'); 
Route::get('/authors/showBiography', [App\Http\Controllers\BiographiesController::class, 'showBiography'])->name('show_biography'); 


Route::get('/content/addFloor', [App\Http\Controllers\FloorsController::class, 'showaddFloors'])->name('show_addFloor');
Route::post('/content/storeFloor', [App\Http\Controllers\FloorsController::class, 'storeFloor'])->name('store_floor_and_hall');

Route::get('/settings/basic', [App\Http\Controllers\BasicController::class, 'show_floors'])->name('basic');
// Route::delete('/settings/basic/{floor}', [App\Http\Controllers\BasicController::class, 'delete_floor'])->name('delete_floor');
Route::post('/settings/basic', [App\Http\Controllers\BasicController::class, 'store_floor'])->name('store_floor');
Route::post('/settings/basic/{floor}', [App\Http\Controllers\BasicController::class, 'edit_floor'])->name('edit_floor');
Route::post('/settings/basic/store_hall/{floor}', [App\Http\Controllers\BasicController::class, 'store_hall'])->name('store_hall');


//Route::view('/content/showArt', 'content.showArt');
Route::get('/content/showArt/{floor}/{hall}', [App\Http\Controllers\HallsController::class, 'showArt'])->name('showArt');
Route::get('/content/addArt/{floor}/{hall}', [App\Http\Controllers\HallsController::class, 'show_addArt'])->name('show_addArt');
Route::post('/content/storeArt/{floor}/{hall}', [App\Http\Controllers\HallsController::class, 'storeArt'])->name('store_art');
Route::get('/content/editArt/{art_id}', [App\Http\Controllers\HallsController::class, 'showEditBladeArt'])->name('show_edit_blade_art'); 
Route::post('/content/edit/{art_id}', [App\Http\Controllers\HallsController::class, 'editArt'])->name('edit_art');//route('edit_art', ['art_id'=>$art->id]) 

Route::get('/content/hall_activity', [App\Http\Controllers\HomeController::class, 'changeActivity'])->name('hall_change_activity');

Route::post('/general/store', [App\Http\Controllers\GeneralInformationController::class, 'store_general_information'])->name('store_general_information');
//gotovo s ubacivanjem

Route::get('/json', [App\Http\Controllers\HomeController::class, 'jsonFile'])->name('json_file');



