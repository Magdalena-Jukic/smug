<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use DB;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert(
            [
                [
                
                    'language' => "bugarski",
                    'short_language' => "bg"

                ], 
                [
                    'language' => "češki",
                    'short_language' => "cs"

                ], 
                [
                    'language' => "danski",
                    'short_language' => "da"

                ],
                [
                    'language' => "engleski",
                    'short_language' => "en"

                ],  
                [
                    'language' => "estonski",
                    'short_language' => "et"

                ], 
                [
                    'language' => "finski",
                    'short_language' => "fi"

                ], 
                [
                    'language' => "francuski",
                    'short_language' => "fr"

                ], 
                [
                    'language' => "grčki",
                    'short_language' => "el"

                ], 
                [
                    'language' => "hrvatski",
                    'short_language' => "hr"

                ], 
                [
                    'language' => "irski",
                    'short_language' => "ga"

                ] 
            ]
        );
    }
}
