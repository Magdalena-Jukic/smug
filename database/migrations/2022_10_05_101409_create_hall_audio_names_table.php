<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hall_audio_names', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hall_id');
            $table->bigInteger('language_id');
            $table->string('audio')->nullable();
            $table->string('name');
            $table->unique(['hall_id','language_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hall_audio_names');
    }
};
