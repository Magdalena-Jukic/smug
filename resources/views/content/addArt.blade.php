@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Sadržaj</h1>
        @foreach($hall_name as $hn)
        <h5>{{$floor->name}}/{{$hn->name}}</h5>
        @endforeach
    <form action="{{ route('store_art', ['floor' => $floor->id, 'hall' => $hall->id]) }}" method="post" enctype="multipart/form-data" id="form1">
        @method('POST')
        @csrf
    </form>
    

        <div class="justify-content-center pt-5" >
            <h5>Dodaj novu umjetninu</h5>
            <div class="border border-dark p-3" >
                <div class="container">

                    <div class="container">
                        <div class="row g-2">

                            <div class="col-6">
                                <div class="p-3 border bg-light">                                
                                    
                                        <div class="row fw-bold fs-5">
                                            <label for="name">Ime i prezime umjetnika</label>
                                        </div>
                                        <div class="row ps-2" >
                                            <input type="text" style="width:250px" name="name" list="name" form="form1" class="form-control">
                                            <datalist id="name">
                                                @foreach($authors as $author)
                                                <option value="{{$author->getAuthorsRelation->name}}">
                                                @endforeach                                               
                                            </datalist>
                                        </div> 
                                    
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="p-3 border bg-light">
                                
                                    <div class="row fw-bold fs-5">
                                        <label for="info">Informacije o umjetnini</label>
                                    </div>
                                    <div class="row ps-2" >
                                        <input type="text" style="width:250px" name="info" placeholder="INFO o umjetnini" form="form1" class="form-control">
                                    </div> 

                                    
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="p-3 border bg-light">
                                
                                    <div class="row fw-bold fs-5">
                                        Fotografija umjetničkog djela
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <img src="" alt="" style="height:120px; width:130px" >
                                        </div>
                                        
                                        <div class="col">
                                            <div class="form-check">
                                            <input class="form-check-input" type="radio" name="layout" id="radioPotret"  value="portret" checked form="form1">
                                            <label class="form-check-label" for="radioPotret">PORTRET</label>
                                            </div>
                                            <div class="form-check">
                                            <input class="form-check-input" type="radio" name="layout" value="pejzaz" id="radioPejzaz" form="form1">
                                            <label class="form-check-label" for="radioPejzaz">PEJZAŽ</label>
                                        </div>

                                        </div>
                                    </div>
                                    <div class="row d-flex justify-content-end">
                                        <div class="col-6">
                                            <input type="file" name="image" form="form1" class="form-control">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <div class="col-6">
                                <div class="p-3 border bg-light">
                                    
                                    <div class="row ps-2" >
                                        
                                    <script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>

                                        <div class="form">
                                            <div class="fw-bold fs-5">
                                                <p>QR Code</p>
                                            </div>
                                        
                                            <form>
                                                <div class="row">
                                                    <div class="col">
                                                        <input type="text" id="qr_artName" name="qr_artName" placeholder="Opći naziv umjetnine" form="form1" class="form-control"/>
                                                    </div>
                                                    <div class="col">
                                                        <button type="button" onclick="generateQRCode()" class="btn btn-light border border-dark" >
                                                            Generate QR Code
                                                        </button>

                                                    </div>

                                                </div>
                                                
                                                <input type="hidden" id="qr_hallName" name="qr_hallName" value="{{$hall->name}}" class="form-control"/>
                                            </form>

                                        <div id="qrcode-container">
                                            <p>
                                                <div id="qrcode" class="qrcode"></div>
                                            </p>
                                        </div>


                                        <script type="text/javascript">

                                            function downloadURI(uri, name) {
                                                var link = document.createElement("a");
                                                link.download = name;
                                                link.href = uri;
                                                document.body.appendChild(link);
                                                link.click();
                                                document.body.removeChild(link);
                                                delete link;
                                            }
                                        
                                            function generateQRCode() {
                                                let qr_artName = document.getElementById("qr_hallName").value + "-" +
                                                                document.getElementById("qr_artName").value;
                                                if (qr_artName) {
                                                    let qrcodeContainer = document.getElementById("qrcode");
                                                    qrcodeContainer.innerHTML = "";
                                                    new QRCode(qrcodeContainer,{
                                                        text: qr_artName,
                                                        width: 128,
                                                        height: 128,
                                                        colorDark: "#000000",
                                                        colorLight: "#ffffff"
                                                    });
                                                    document.getElementById("qrcode-container").style.display = "block";
                                                } else {
                                                    alert("Unesite naziv djela!");
                                                }

                                                setTimeout(
                                                    function ()
                                                    {
                                                        let dataUrl = document.querySelector('#qrcode').querySelector('img').src;
                                                        downloadURI(dataUrl, qr_artName+'.png');
                                                    }
                                                    ,1000);
                                            }

                                        </script>
                                        </div>
                                        
                                    </div> 

                                    
                                </div>
                            </div>
                        
                            
                            @foreach($languages as $language)
                            <div class="col-6">
                                <div class="p-3 border bg-light">
                                
                                    <div class="row fw-bold fs-5">
                                        <label for="artName">Naziv umjetnine</label>
                                        <input type="hidden" name="language_id[]" value="{{$language->id}}" form="form1" class="form-control"> 
                                    </div>
                                    <div class="row ps-2" >
                                        <input type="text" style="width:250px" name="artName[]" 
                                            placeholder="{{$language->getLanguageRelation->language}} jezik" 
                                            form="form1"
                                            class="form-control">
                                    </div> 
                                    
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>

                </div>
            </div>
        </div>

        
        
        <div class="row p-3">
            
            <div class="p-4">
                
                <div class="container">
                    <div class="row justify-content-evenly">
                        <div class="col-1">
                            <button class="btn btn-light border border-dark" form="form1">SPREMITE</button>
                        </div>
                    </div>
                </div>

            </div>    
        </div>
    

    
    </div>
</div>

@endsection