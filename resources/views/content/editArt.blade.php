@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Popis umjetnina</h1>
        <form action="{{ route('edit_art', ['art_id'=>$art->id]) }}" method="post" enctype="multipart/form-data">
        @method('POST')
        @csrf

        <div class="justify-content-center" >
            <h5>Izmjena podataka umjetnine</h5>
            <div class="border border-dark p-3" >
                <div class="container">

                    <div class="container">
                        <div class="row g-2">

                            <div class="col-6">
                                <div class="p-3 border bg-light">                                
                                    
                                    <div class="col">
                                        <div class="row fw-bold fs-5">
                                            <label for="author_name">Umjetnik</label>
                                        </div>
                                        <div class="row ps-2" >
                                            <input class="form-control" name="author_name" id="author_name" value="{{$art->getAuthorRelation->name}}">
                                        </div> 
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="p-3 border bg-light">                                
                                    
                                <div class="row fw-bold fs-5">
                                        Fotografija umjetnine
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <!-- <img style="height:120px; width:130px;" > -->
                                            <img src="{{ $art->image }}" alt="{{$art->image}}" style="height:120px; width:130px">
                                        </div>
                                        <div class="col">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="layout" id="radioPotret"  value="portret" checked>
                                                <label class="form-check-label" for="radioPotret">PORTRET</label>
                                                </div>
                                                <div class="form-check">
                                                <input class="form-check-input" type="radio" name="layout" value="pejzaz" id="radioPejzaz">
                                                <label class="form-check-label" for="radioPejzaz">PEJZAŽ</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row d-flex justify-content-end">
                                        <div class="col-6">
                                            <input type="file" name="image" class="form-control">
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div class="row g-2"> 
                            @foreach($art_name as $an)
                                <div class="col-6">
                                    <div class="p-3 border bg-light">                                
                                        
                                        <div class="col">
                                            <div class="row fw-bold fs-5">
                                                <label for="language_name">Naziv umjetnine ({{App\Models\Language::find($an->getLanguageRelation->language_id)->language}} jezik)</label>
                                                
                                            </div>
                                            <div class="row ps-2">
                                                <input class="form-control" name="language_name" id="language_name" value="{{$an->name}}">                         
                                            </div> 
                                        </div>
                                                                               
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    
                       
                    </div>
                </div>
            </div>
        
        </div>

        <div class="row p-3">
            
            <div class="p-4">
                
                <div class="container">
                    <div class="row justify-content-evenly">
                        <div class="col-3">
                            <button class="btn btn-light border border-dark">SPREMITE IZMJENE</button>
                        </div>
                    </div>
                </div>

            </div>    
        </div>
    </form>
        
    </div>
</div>


@endsection
