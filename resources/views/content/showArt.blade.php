@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Popis umjetnina</h1>
        <div class="pt-5">
            <h3>{{$floor->name}}</h3>
            @foreach($hall as $h)
            <h5>Dodaj sadržaj za dvoranu: {{$h->name}}</h5>
            <div class="border border-dark p-3">
                <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 bg-light" style="max-width: 1000px; max-height: 700px; height: 500px;">
                                    
                <div class="row">
                    @foreach($arts as $art)

                        <div class="col-lg-2 p-3">
                            <div class="row d-flex justify-content-center">
                                <a href="{{ route('show_edit_blade_art', ['art_id'=> $art->id])}}" style="img-decoration:none">
                                    <img src="{{ $art->image }}" alt="{{$art->image}}" style="height:120px; width:130px" class="rounded-circle">
                                </a>
                            </div>
                            <div class="row d-flex justify-content-center pt-1 text-center">
                                <a href="{{ route('show_edit_blade_art', ['art_id'=> $art->id])}}" style="text-decoration:none; color:black;" class="d-flex justify-content-center">
                                    {{$art->getArtNameLanguageRelation->where("language_id",1)->first()->name}}
                                </a>
                            </div> 
                        </div>  
                       
                    @endforeach 
                    
                        <div class="col-lg-2 p-3">
                            <div class="row d-flex justify-content-center">
                                <div>
                                    <a href="{{ route('show_addArt', ['floor'=> $floor->id, 'hall'=>$h->hall_id]) }}">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; height:120px; width:140px" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M256,0C114.844,0,0,114.844,0,256c0,141.156,114.844,256,256,256s256-114.844,256-256C512,114.844,397.156,0,256,0z
                                                        M256,490.667C126.604,490.667,21.333,385.397,21.333,256C21.333,126.606,126.604,21.333,256,21.333
                                                        c129.396,0,234.667,105.272,234.667,234.667C490.667,385.397,385.396,490.667,256,490.667z"/>
                                                </g>
                                            </g>
                                            <g>
                                                <g>
                                                    <polygon points="265.562,246.05 265.562,96.716 244.229,96.716 244.229,246.05 105.562,246.05 105.562,267.383 244.229,267.383 
                                                        244.229,416.716 265.562,416.716 265.562,267.383 414.896,267.383 414.896,246.05 		"/>
                                                </g>
                                            </g>
                                        </svg>  
                                    </a>

                                </div> 
                            </div>

                            <div class="row d-flex justify-content-center pt-1 text-center">
                                <a href="{{ route('show_addArt', ['floor'=> $floor->id, 'hall'=>$h->id]) }}" style="text-decoration: none; color: black;">
                                    Dodaj umjetninu
                                </a>
                            </div> 
                        </div>
                    </div>
                    
                </div>
            </div>
            @endforeach
        </div>
                
    </div>
</div>


@endsection

