@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Popis autora i biografija</h1>
        <form action="{{ route('store_author') }}" method="post" enctype="multipart/form-data">
        @method('POST')
        @csrf

        <div class="justify-content-center pt-5" >
            <h5>Dodaj novog autora / umjetnika</h5>
            <div class="border border-dark p-3" >
                <div class="container">

                    <div class="container">
                        <div class="row g-2">

                            <div class="col-6">
                                <div class="p-3 border bg-light">                                
                                    
                                        <div class="row fw-bold fs-5">
                                            <label for="name">Ime i prezime umjetnika</label>
                                        </div>
                                        <div class="row ps-2" >
                                            <input type="text" style="width:250px" name="name" class="form-control">
                                        </div> 
                                    
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="p-3 border bg-light">
                                
                                    <div class="row fw-bold fs-5">
                                        Fotografija umjetnika
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <img src="" alt="" style="height:120px; width:130px;" >
                                        </div>
                                        <div class="col">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="layout" id="radioPotret"  value="portret" checked>
                                                <label class="form-check-label" for="radioPotret">PORTRET</label>
                                                </div>
                                                <div class="form-check">
                                                <input class="form-check-input" type="radio" name="layout" value="pejzaz" id="radioPejzaz">
                                                <label class="form-check-label" for="radioPejzaz">PEJZAŽ</label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row d-flex justify-content-end">
                                        <div class="col-6">
                                            <input type="file" name="image" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row g-2">
                            @foreach($languages as $lan)
                                    <div class="col-6">
                                        <div class="p-3 border bg-light">                                
                                            
                                            <div class="col">
                                                <div class="row fw-bold fs-5">
                                                    <label for="name">Biografija ({{$lan->getLanguageRelation->language}} jezik)</label>
                                                    <input type="hidden" name="language_id[]" value="{{$lan->language_id}}"> 
                                                </div>
                                                <div class="row ps-2" >
                                                    <textarea name="biography[]" id="biography" cols="30" rows="10" class="form-control"></textarea>                              
                                                </div> 
                                            </div>
                                        
                                        </div>
                                    </div>
                            @endforeach
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row p-3">
            
            <div class="p-4">
                
                <div class="container">
                    <div class="row justify-content-evenly">
                        <div class="col-1">
                            <button class="btn btn-light border border-dark">SPREMITE</button>
                        </div>
                    </div>
                </div>

            </div>    
        </div>
    </form>
        
    </div>
</div>


@endsection
