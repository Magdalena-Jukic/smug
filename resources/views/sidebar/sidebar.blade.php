<div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
    <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">

        <ul class="flex-md-column flex-row navbar-nav w-100 justify-content-between pt-3">
            <li>
                <div class="row">

                    <div class="col">
                        <a href="{{ route('welcome')}}">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 55 55" style="enable-background:new 0 0 27.442 27.442; color:white;" xml:space="preserve" class="w-8 h-8"
                                    fill="white"
                                >
                                <g>
                                    <g>
                                        <path d="M42.759,43.849C42.569,43.627,42.292,43.5,42,43.5H13c-0.292,0-0.569,0.127-0.759,0.349l-6,7
                                            c-0.254,0.296-0.313,0.714-0.149,1.069C6.255,52.273,6.609,52.5,7,52.5h41c0.391,0,0.745-0.228,0.909-0.582
                                            c0.163-0.355,0.104-0.772-0.149-1.069L42.759,43.849z M9.174,50.5l4.286-5h28.08l4.286,5H9.174z"/>
                                        <rect x="50" y="49.5" width="5" height="2"/>
                                        <rect y="49.5" width="5" height="2"/>
                                        <rect x="2" y="44.5" width="5" height="2"/>
                                        <rect x="48" y="44.5" width="5" height="2"/>
                                        <path d="M18,17.5h-6c-0.5,0-0.923,0.37-0.991,0.865C10.968,18.667,10,25.808,10,29.5c0,3.692,0.968,10.833,1.009,11.135
                                            C11.077,41.13,11.5,41.5,12,41.5h6c0.5,0,0.923-0.37,0.991-0.865C19.032,40.333,20,33.192,20,29.5
                                            c0-3.692-0.968-10.833-1.009-11.135C18.923,17.87,18.5,17.5,18,17.5z M17.122,39.5h-4.244C12.618,37.462,12,32.323,12,29.5
                                            c0-2.822,0.618-7.962,0.878-10h4.244c0.26,2.037,0.878,7.177,0.878,10C18,32.323,17.382,37.462,17.122,39.5z"/>
                                        <path d="M31,17.5h-6c-0.5,0-0.923,0.37-0.991,0.865C23.968,18.667,23,25.808,23,29.5c0,3.692,0.968,10.833,1.009,11.135
                                            C24.077,41.13,24.5,41.5,25,41.5h6c0.5,0,0.923-0.37,0.991-0.865C32.032,40.333,33,33.192,33,29.5
                                            c0-3.692-0.968-10.833-1.009-11.135C31.923,17.87,31.5,17.5,31,17.5z M30.122,39.5h-4.244C25.618,37.462,25,32.323,25,29.5
                                            c0-2.822,0.618-7.962,0.878-10h4.244c0.26,2.037,0.878,7.177,0.878,10C31,32.323,30.382,37.462,30.122,39.5z"/>
                                        <path d="M44,17.5h-6c-0.5,0-0.923,0.37-0.991,0.865C36.968,18.667,36,25.808,36,29.5c0,3.692,0.968,10.833,1.009,11.135
                                            C37.077,41.13,37.5,41.5,38,41.5h6c0.5,0,0.923-0.37,0.991-0.865C45.032,40.333,46,33.192,46,29.5
                                            c0-3.692-0.968-10.833-1.009-11.135C44.923,17.87,44.5,17.5,44,17.5z M43.122,39.5h-4.244C38.618,37.462,38,32.323,38,29.5
                                            c0-2.822,0.618-7.962,0.878-10h4.244c0.26,2.037,0.878,7.177,0.878,10C44,32.323,43.382,37.462,43.122,39.5z"/>
                                        <path d="M7,16.5h42c0.454,0,0.85-0.305,0.966-0.743c0.117-0.439-0.077-0.9-0.47-1.125l-21-12c-0.308-0.176-0.685-0.176-0.992,0
                                            l-21,12c-0.394,0.225-0.587,0.686-0.47,1.125C6.15,16.195,6.546,16.5,7,16.5z M28,4.652L45.234,14.5H10.766L28,4.652z"/>
                                    </g>
                                </g>
                            
                            </svg>
                        </a>
                    </div>
                    <div class="col d-flex align-items-center">
                        <a href="{{ route('welcome')}}" class="text-decoration-none link-light">
                            SMUG
                        </a>
                    </div>
                </div>
            </li>
            <hr>
            <li class="nav-item has-submenu">
                <a href="{{ route('home') }}" class="nav-link pl-0 text-nowrap"><span class="d-none d-md-inline">Nadzorna ploča</span></a>
            </li>
            <li class="nav-item">
                <a href="#collapseSettings" class="nav-link pl-0 text-nowrap" data-bs-toggle="collapse"><span class="d-none d-md-inline">Postavke</span></a>
                    <ul class="submenu collapse" id="collapseSettings">
                        <li><a class="nav-link" href="{{ route('basic') }}">Osnovne postavke</a></li>
                        <li><a class="nav-link" href="{{ route('localization') }}">Postavke mobilne aplikacije</a></li>
                        <li><a class="nav-link" href="{{ route('users') }}">Korisnici</a></li>
                        <li><a class="nav-link" href="{{ route('language') }}">Jezici</a> </li>
                    </ul>
            </li>
            <li class="nav-item">
                <a href="#collapseContent" class="nav-link pl-0 text-nowrap" data-bs-toggle="collapse"><span class="d-none d-md-inline">Sadržaj</span></a>
                    <ul class="submenu collapse" id="collapseContent">
                        @foreach($floors as $floor)
                            @if(auth()->user()->institution_id == $floor->institution_id)
                            <li><a class="nav-link pl-0 text-nowrap" href="#collapseFloors" data-bs-toggle="collapse">{{$floor->name}}</a>
                                <ul class="submenu collapse" id="collapseFloors">
                                    @foreach($halls as $hall)
                                        @if($hall->getHallRelation->active == 1)
                                            @if($floor->id == $hall->getHallRelation->floor_id && $floor->institution_id == $hall->getHallRelation->institution_id)
                                            @if($hall->language_id == 9)
                                            <li><a class="nav-link" href="{{ route('showArt', ['floor' => $floor->id, 'hall' => $hall->hall_id]) }}">{{$hall->name}}</a></li>
                                            @endif
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                            @endif
                        @endforeach
                    </ul>
            </li>
            <li class="nav-item">
                <a  href="#collapseAuthor" class="nav-link pl-0 text-nowrap" data-bs-toggle="collapse"><span class="d-none d-md-inline">Popis autora</span></a>
                    <ul class="submenu collapse" id="collapseAuthor">
                        <li><a class="nav-link" href="{{ route('authors') }}">Autori</a></li>
                    </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('json_file') }}" class="nav-link pl-0 text-nowrap"><span class="d-none d-md-inline">Objava sadržaja</span></a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link pl-0 text-nowrap">
                    <div class="d-md-inline">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle">
                            <circle cx="12" cy="12" r="10"></circle>
                            <path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path>
                            <line x1="12" y1="17" x2="12.01" y2="17"></line>
                        </svg>
                    </div>
                    <span class="d-none d-md-inline">Pomoć</span>
                </a>
            </li>       
            <hr> 

            <li>
                {{$institution->name}}

            </li>     
        </ul>

    </div>
</div>

<style>
    li{
        list-style: none;
    }

</style>

