@extends('layouts.app')

@section('content')

<div class="container p-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Registracija') }}</div>
                    
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            @method('POST')

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Ime i prezime') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('E-pošta') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Lozinka') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Potvrda Lozinke') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="phone" class="col-md-4 col-form-label text-md-end">{{ __('Telefeon / Mobitel') }}</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="role" class="col-md-4 col-form-label text-md-end">{{ __('Uloga') }}</label>
                                <div class="col-md-6">
                                    <div>
                                        <input type="radio" id="role_admin" name="role" value="1" class="form-check-input">
                                        <label for="admin" class="form-check-label">Admin</label>
                                    </div>
                                    <div>
                                        <input type="radio" id="role_editor" name="role" value="2" class="form-check-input">
                                        <label for="editor" class="form-check-label">Urednik</label>
                                    </div>
                                </div>
                                @error('role')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                           
                            <div class="row mb-3">
                                <label for="institution" class="col-md-4 col-form-label text-md-end">{{ __('Institucija') }}</label>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <select name="institution" id="institution" class="form-control">
                                        <option  selected disabled>Odaberi instituciju</option>
                                        @foreach($institutions as $institution)
                                        <option value="{{$institution->id}}">{{$institution->name}}</option>
                                        @endforeach                                            
                                        </select>
                                        <a class="btn btn-dark" type="button" href="{{ route('institution') }}">Dodajte</a>
                                    </div>                                       
                                </div>
                                  
                            </div>


                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-dark">
                                        {{ __('Registirajte se') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>
                <script>
                    var openForm = document.getElementById("institution");
                    var value = openForm.value;
                    var show = document.getElementById("ovo");
                    if(value == "0"){
                        show.style.display = "compact";
                    } else{
                        show.style.display = "none";
                    }
                    
                </script>
            </div>
        </div>
    </div>

@endsection
