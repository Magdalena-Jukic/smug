@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Nadzorna ploča</h1>
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8 py-5">
                <div class="container">
                <form action="{{ route('hall_change_activity') }}">
                    <table class="table">
                        <thead>
                            <tr>
                                <td><input type="checkbox" onclick="toggle(this);">  Odaberi sve</td>
                                <td>KAT / DVORANA</td>
                                <td>Aktivnost</td>
                            </tr>
                        </thead>
                        
                        <tbody>
                        @foreach($halls as $hall)                        
                            <tr>
                                <td><input type="checkbox" name="checkbox[]" value="{{$hall->id}}"></td>
                                @foreach($hall_names as $name)
                                @if($hall->id == $name->hall_id && $name->language_id == 9)
                                <td> {{$hall->getFloorRelation->name}} / {{$name->name}} </td>
                                @endif
                                @endforeach
                                
                                @if($hall->active == 1)   
                                <td>
                                    <svg width="32px" height="32px" viewBox="0 0 32 32" id="icon" xmlns="http://www.w3.org/2000/svg">
                                        <defs>
                                            <style>
                                            .cls-1 {
                                                fill: none;
                                            }
                                            </style>
                                        </defs>
                                        <title>checkbox--checked--filled</title>
                                        <path d="M26,4H6A2,2,0,0,0,4,6V26a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V6A2,2,0,0,0,26,4ZM14,21.5,9,16.5427,10.5908,15,14,18.3456,21.4087,11l1.5918,1.5772Z" transform="translate(0 0)"/>
                                        <path id="inner-path" class="cls-1" d="M14,21.5,9,16.5427,10.5908,15,14,18.3456,21.4087,11l1.5918,1.5772Z" transform="translate(0 0)"/>
                                        <rect id="_Transparent_Rectangle_" data-name="&lt;Transparent Rectangle&gt;" class="cls-1" width="32" height="32"/>
                                    </svg>
                                    
                                </td>
                                @else
                                <td>
                                    <svg width="32px" height="32px" viewBox="0 0 32 32" id="icon" xmlns="http://www.w3.org/2000/svg">
                                        <defs>
                                            <style>
                                                .cls-1 {
                                                    fill:none;
                                                }
                                            </style>
                                        </defs>
                                        <title>checkbox--indeterminate--filled</title>
                                        <path d="M26,4H6A2,2,0,0,0,4,6V26a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V6A2,2,0,0,0,26,4ZM22,18H10V14H22Z"/>
                                        <rect id="_Transparent_Rectangle_" data-name="&lt;Transparent Rectangle&gt;" class="cls-1" width="32" height="32"/>
                                    </svg>
                                </td>
                                @endif
                            </tr>
                        
                        @endforeach


                        </tbody>
                    </table>

                    <div class="row p-3">
            
                        <div class="p-4">
                            
                            <div class="container">
                                <div class="row justify-content-evenly">
                                    <div class="col-1">
                                        <button class="btn btn-light border border-dark">SPREMITE</button>
                                    </div>
                                </div>
                            </div>

                        </div>    
                    </div>
                </form>

                </div>
            </div>
        </div>     
    </div>
</div>
<script>
    function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
</script>

@endsection
