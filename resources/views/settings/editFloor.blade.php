@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif

        <h1>Postavke</h1>
        <form action="{{ route('store_hall', ['floor'=>$floor->id]) }}" method="post" enctype="multipart/form-data">
        @method('POST')
        @csrf

        <div class="justify-content-center" >
            <h5>{{$floor->name}}</h5>
            <div class="border border-dark p-3" >
                <div class="container">

                    <div class="container">

                        <div class="row g-2">
                        @foreach($languages as $lan)
                            <div class="col-6">
                                <div class="p-3 border bg-light">
                                    <div class="col">
                                        <div class="row fw-bold fs-5">
                                            <label for="name">Naziv dvorane</label>
                                        </div>
                                        <div class="row p-2 g-2">
                                            <input type="text" name="name[]" class="col-6 form-control" placeholder="Naziv dvorane ({{$lan->getLanguageRelation->language}} jezik)">
                                        </div>
                                          
                                    </div> 
                                </div>
                            </div>
                        @endforeach
                        </div>

                        <div class="row g-2">
                            <div class="col-6">
                                <div class="p-3 border bg-light">
                                    <div class="row fw-bold fs-5">
                                        <label for="image">Fotografija</label>
                                    </div>
                                    <div class="row">
                                        <div class="col-6"><img src="" alt="" style="height:120px; width:130px" ></div>
                                        <div class="col-6 d-flex align-items-center"><input type="file" name="image" class="form-control"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="row g-2">
                        @foreach($languages as $lan)
                            <div class="col-6">
                                
                                <div class="p-3 border bg-light">
                                    <div class="row fw-bold fs-5">
                                        <label for="audio">Dodaj sadržaj za {{$lan->getLanguageRelation->language}} jezik</label>
                                    </div>
                                    <div class="row p-4">
                                        <div class="col-6">
                                            <div class="row"> 
                                                <audio controls="" style="vertical-align: middle" src="" type="audio/mp3" controlslist="nodownload">
                                                    Your browser does not support the audio element.
                                                </audio>
                                            </div>  
                                        </div>
                                        <div class="col-6 d-flex align-items-center"><input type="file" name="audio[]" class="form-control"></div>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                        </div>
                       
                    </div>
                </div>
            </div>
        
        </div>

        <div class="row p-3">
            
            <div class="p-4">
                
                <div class="container">
                    <div class="row justify-content-evenly">
                        <div class="col-3">
                            <button class="btn btn-light border border-dark form-control">SPREMITE IZMJENE</button>
                        </div>
                    </div>
                </div>

            </div>    
        </div>
    </form>
        
    </div>
</div>


@endsection
