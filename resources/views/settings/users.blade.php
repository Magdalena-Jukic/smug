@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Postavke</h1>
        <div class="row justify-content-center">
            <div class="col-md-8 py-5">
            <h3>Korisnici</h3>
            <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 bg-light" style="max-width: 1000px; max-height: 400px; height: 400px;">

                <!-- <h5>Admin</h5> -->

                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Ime i prezime</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Uloge</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                        
                                       
                        <tbody>
                        @foreach($users_admin as $admin) 
                                <tr>
                                    <td></td>
                                    <td>{{$admin->name}}</td>
                                    <td>{{$admin->email}}</td>
                                    <td>Admin</td>
                                    <td></td>
                                    <td>
                                    <!-- @if(auth()->user()->role == 1)
                                        
                                        <a href="#" style="text-decoration:none; color:black; width:60px " class="btn d-flex justify-content-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>
                                                <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>
                                            </svg>
                                        </a>
                                        
                                    @endif   -->
                                    </td>
                                </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                       
                        @foreach($users_editor as $editor)
                               
                                <tr>
                                    <td></td>
                                    <td>{{$editor->name}}</td>
                                    <td>{{$editor->email}}</td>
                                    <td>Urednik</td>
                                    <td></td>
                                
                                    <td>
                                    <!-- @if(auth()->user()->role == 1)  
                                
                                        <form action="#" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('DELETE')
                                        
                                            <button type="submit" class="btn">
                                                <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" width="24px" height="24px">
                                                    <path d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L 16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 7 L 4.3652344 7 z"/>
                                                </svg>
                                            </button>
                                        
                                        </form>
                                    @endif -->
                                    </td>
                                </tr> 
                                @endforeach 
                            
                        </tbody>
                        
                    
                </table>

            </div>


            </div>
        </div>     
    </div>
</div>


@endsection