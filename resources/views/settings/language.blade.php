@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Postavke</h1>
        <div class="row justify-content-center">
            <div class="col-md-8 py-5">
                <div class="container">
                <h3>Jezici</h3>

                <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 bg-light" style="max-width: 1000px; max-height: 300px; height: 300px;">
                    <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($language_inst as $lan)
                        <tr>
                            <td></td>
                            <td>{{strtoupper($lan->getLanguageRelation->language)}} JEZIK</td>
                            <td></td>
                            <td>
                            <form action="{{ route('delete_language', ['language'=> $lan->id]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('DELETE')

                                <button class="btn">
                                    <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" width="24px" height="24px">
                                        <path d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L 16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 7 L 4.3652344 7 z"/>
                                    </svg>
                                </button>
                            </form>
                            </td>
                        </tr>
                        @endforeach
                        

                    </tbody>
                    </table>
                </div>


                    <form action="{{ route('store_language') }}" method="post" class="py-5">
                    @csrf
                        <div class="input-group">
                            <input list="languages" name="languages" type="text" class="form-control">
                        
                            <datalist id="languages" name="languages">
                                @foreach($language as $lan)
                                <option value="{{strtoupper($lan->language)}} JEZIK">
                                @endforeach
                            </datalist>
                        
                            <button class="btn btn-dark">Dodajte</button>
                        </div>
                                
                    </form>   
                    
                    
                </div>
            </div>
        </div>     
    </div>
</div>


@endsection

