@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        
        @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <h1>Postavke</h1>
        <div class="row justify-content-center">
            <div class="col-md-8 py-5">
                <div class="container">
                <h3>Osnovne postavke</h3>
                    <div class="pt-3">
                                            
                        <form action="{{ route('store_general_information') }}" method="post" enctype="multipart/form-data">
                        @method('POST')
                        @csrf

                        
                            <div class="row p-3">
                                <div class="row fw-bold fs-5">
                                    <label for="image">Naslovna fotografija</label>
                                </div>
                                <div class="row p-5" style="border-style: dotted;">
                                    <div class="col d-flex align-items-center"><input type="file" name="image" class="form-control" onchange="loadFile(this)"></div>
                                    <div class="col">
                                        <img src="{{$general_information->image}}" alt="{{$general_information->image}}" style="height:120px; width:130px;" >
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row p-3">
                                <div class="row fw-bold fs-5">
                                    <label for="logo">Logo</label>
                                </div>
                                <div class="row p-5" style="border-style: dotted;">
                                    <div class="col d-flex align-items-center"><input type="file" name="logo" class="form-control"></div>
                                    <div class="col">
                                        <img src="{{$general_information->logo}}" alt="{{$general_information->logo}}" style="height:120px; width:130px;" >
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row p-3">
                                <div class="row fw-bold fs-5">
                                    <label for="info_text">INFO tekst</label>
                                </div>
                                <div class="row p-5">
                                    <textarea type="text" name="info_text" cols="30" rows="10" placeholder="INFO tekst" class="form-control">{{$general_information->info_text}}</textarea>
                                </div>
                            </div>

                            <div class="row p-3">
                                <div class="row fw-bold fs-5">
                                    <label for="audio">Audio sadržaj / glazbena podloga aplikacije</label>
                                </div>
                                <div class="row p-5" style="border-style: dotted;">
                                    <div class="col d-flex align-items-center"><input type="file" name="audio" class="form-control"></div>
                                    <div class="col"> 
                                        <div class="row">  
                                            <audio controls="" style="vertical-align: middle" src="{{$general_information->audio}}" type="audio/mp3" controlslist="nodownload">
                                                Your browser does not support the audio element.
                                            </audio>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                            

                            <div class="d-flex justify-content-center p-3">
                                <button class="btn btn-dark" >SPREMITE</button>
                            </div>
                        </form> 

                        <div class="border border-dark p-3">

                            <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 bg-light" style="max-width: 1000px; max-height: 300px; height: 300px;">
                                <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($floors as $floor)
                                    <tr>
                                        <td></td>
                                        <td>{{$floor->name}}</td>
                                        <td></td>
                                        <td>                                
                                            <!-- <form action="#" method="post" enctype="multipart/form-data">
                                            @csrf
                                            @method('DELETE')
                                                    
                                                <button class="btn">
                                                    <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" width="24px" height="24px">
                                                        <path d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L 16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 7 L 4.3652344 7 z"/>
                                                    </svg>
                                                </button>
                                                    
                                            </form> -->
                                        </td>
                                        <td>
                                            <form action="{{ route('edit_floor', ['floor'=>$floor->id]) }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                @method('POST')
                                                        
                                                <button class="btn">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>
                                                        <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>
                                                    </svg>

                                                </button>
                                                                
                                            </form>

                                        </td>
                                        
                                        
                                    </tr>
                                    @endforeach

                                </tbody>
                                </table>
                            </div>


                            <form action="{{ route('store_floor') }}" method="post" class="py-5">
                            @csrf
                            @method('POST')
                                <div class="input-group">
                                    <input name="floor" type="text" class="form-control" placeholder="Ime kata">         
                                    <button class="btn btn-dark">Dodajte</button>
                                </div>
                                        
                            </form>   
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </div>
</div>


@endsection

