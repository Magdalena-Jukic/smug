@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Postavke</h1>
        <div class="row justify-content-center">
            <div class="col-md-8 py-5">
            <h3>Postavke mobilne aplikacije</h3>

                <form action="{{ route('store_localization') }}" method="post" enctype="multipart/form-data">
                @method('POST')
                @csrf
                    @foreach($languages as $language)
                    <div>
                        <div class="border border-dark p-3 m-3">
                        <div class="fw-bold">{{strtoupper($language->getLanguageRelation->language)}} JEZIK</div>
                            <div class="container p-2">

                                <div class="container">
                                    <div class="row g-2">

                                        <div class="col-6">
                                            <div class="p-3 border bg-light">                                
                                                
                                                <div class="col">
                                                    <div class="row">
                                                        <label for="welcomeText">Dobrodošli</label>
                                                    </div>
                                                    <div class="row ps-2" >
                                                        <input class="form-control" name="welcomeText[]" id="welcomeText" placeholder="Dobrodošli">
                                                    </div> 
                                                </div>
                                                

                                            </div>
                                        </div>

                                    </div>

                                    <div class="row g-2">
                                    
                                            <div class="col-6">
                                                <div class="p-3 border bg-light">                                
                                                    
                                                    <div class="col">
                                                        <div class="row">
                                                            <label for="entry">ULAZ</label>
                                                        </div>
                                                        <div class="row ps-2" >
                                                            <input class="form-control" name="entry[]" id="entry" placeholder="ULAZ">
                                                        </div> 
                                                    </div>
                                                
                                                    
                                                </div>
                                            </div>
                                    
                                    </div>

                                    <div class="row g-2">
                                    
                                            <div class="col-6">
                                                <div class="p-3 border bg-light">                                
                                                    
                                                    <div class="col">
                                                        <div class="row">
                                                            <label for="exit">IZLAZ</label>
                                                        </div>
                                                        <div class="row ps-2" >
                                                            <input class="form-control" name="exit[]" id="exit" placeholder="IZLAZ">
                                                        </div> 
                                                    </div>
                                                
                                                    
                                                </div>
                                            </div>
                                    
                                    </div>

                                    <div class="row g-2">
                                    
                                            <div class="col-6">
                                                <div class="p-3 border bg-light">                                
                                                    
                                                    <div class="col">
                                                        <div class="row">
                                                            <label for="backToStart">Povratak na početak</label>
                                                        </div>
                                                        <div class="row ps-2" >
                                                            <input class="form-control" name="backToStart[]" id="backToStart" placeholder="Povratak na početak">
                                                        </div> 
                                                    </div>
                                                
                                                    
                                                </div>
                                            </div>
                                    
                                    </div>

                                    <div class="row g-2">
                                    
                                            <div class="col-6">
                                                <div class="p-3 border bg-light">                                
                                                    
                                                    <div class="col">
                                                        <div class="row">
                                                            <label for="next">Naprijed</label>
                                                        </div>
                                                        <div class="row ps-2" >
                                                            <input class="form-control" name="next[]" id="next" placeholder="NAPRIJED">
                                                        </div> 
                                                    </div>
                                                
                                                    
                                                </div>
                                            </div>
                                    
                                    </div>

                                    <div class="row g-2">
                                    
                                            <div class="col-6">
                                                <div class="p-3 border bg-light">                                
                                                    
                                                    <div class="col">
                                                        <div class="row">
                                                            <label for="startTour">Kreni u obilazak</label>
                                                        </div>
                                                        <div class="row ps-2" >
                                                            <input class="form-control" name="startTour[]" id="startTour" placeholder="Kreni u obilazak">
                                                        </div> 
                                                    </div>
                                                
                                                    
                                                </div>
                                            </div>
                                    
                                    </div>

                                    <div class="row g-2">
                                    
                                            <div class="col-6">
                                                <div class="p-3 border bg-light">                                
                                                    
                                                    <div class="col">
                                                        <div class="row">
                                                            <label for="atHall">Trenutna lokacija</label>
                                                        </div>
                                                        <div class="row ps-2" >
                                                            <input class="form-control" name="atHall[]" id="atHall" placeholder="Trenutno ste na lokaciji">
                                                        </div> 
                                                    </div>
                                                
                                                    
                                                </div>
                                            </div>
                                    
                                    </div>

                                    <div class="row g-2">
                                    
                                            <div class="col-6">
                                                <div class="p-3 border bg-light">                                
                                                    
                                                    <div class="col">
                                                        <div class="row">
                                                            <label for="chooseHall">Odaberi dvoranu</label>
                                                        </div>
                                                        <div class="row ps-2" >
                                                            <input class="form-control" name="chooseHall[]" id="chooseHall" placeholder="U koju dvoranu želite ući?">
                                                        </div> 
                                                    </div>
                                                
                                                    
                                                </div>
                                            </div>
                                    
                                    </div>
                                
                                
                                </div>
                            </div>
                        </div>
                    
                    </div>
                    @endforeach

                    <div class="row p-3">
                        
                        <div class="p-4">
                            
                            <div class="container">
                                <div class="row justify-content-evenly">
                                    <div class="col-3">
                                        <button class="btn btn-light border border-dark">SPREMITE</button>
                                    </div>
                                </div>
                            </div>

                        </div>    
                    </div>
                </form>

            </div>
        </div>     
    </div>
</div>

@endsection