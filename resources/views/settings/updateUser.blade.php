@extends('layouts.app')

@section('content')
@include('sidebar.sidebar')

<div class="col py-5 px-5">
    <div class="container">
        <h1>Postavke</h1>
        <form action="#" method="post" enctype="multipart/form-data">
        @method('POST')
        @csrf

        <div class="justify-content-center" >
            <h5>Izmjena podataka</h5>
            <div class="border border-dark p-3" >
                <div class="container">

                    <div class="container">
                        <div class="row g-2">

                            <div class="col-6">
                                <div class="p-3 border bg-light">                                
                                    
                                    <div class="col">
                                        <div class="row">
                                            <label for="name">Admin</label>
                                        </div>
                                        <div class="row ps-2" >
                                            <input class="form-control" name="name" id="Name" value="{{$user->name}}">
                                        </div> 
                                    </div>
                                    

                                </div>
                            </div>

                        </div>

                        <div class="row g-2">
                        
                                <div class="col-6">
                                    <div class="p-3 border bg-light">                                
                                        
                                        <div class="col">
                                            <div class="row">
                                                <label for="email">Email</label>
                                            </div>
                                            <div class="row ps-2" >
                                                <input class="form-control" name="email" id="email" value="{{$user->email}}">
                                            </div> 
                                        </div>
                                       
                                        
                                    </div>
                                </div>
                        
                        </div>

                        <div class="row g-2">
                        
                                <div class="col-6">
                                    <div class="p-3 border bg-light">                                
                                        
                                        <div class="col">
                                            <div class="row">
                                                <label for="phone">Broj telefona</label>
                                            </div>
                                            <div class="row ps-2" >
                                                <input class="form-control" name="phone" id="phone" value="{{$user->phone}}">
                                            </div> 
                                        </div>
                                       
                                        
                                    </div>
                                </div>
                        
                        </div>

                        <div class="row g-2">
                        
                                <div class="col-6">
                                    <div class="p-3 border bg-light">                                
                                        
                                        <div class="col">
                                            <div class="row">
                                                <label for="password">Dodaj novu lozinku</label>
                                            </div>
                                            <div class="row ps-2" >
                                                <input class="form-control" name="password" id="password" >
                                            </div> 
                                        </div>
                                       
                                        
                                    </div>
                                </div>
                        
                        </div>
                    
                       
                    </div>
                </div>
            </div>
        
        </div>

        <div class="row p-3">
            
            <div class="p-4">
                
                <div class="container">
                    <div class="row justify-content-evenly">
                        <div class="col-3">
                            <button class="btn btn-light border border-dark">SPREMITE IZMJENE</button>
                        </div>
                    </div>
                </div>

            </div>    
        </div>
    </form>
        
    </div>
</div>


@endsection
