<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Language;
use App\Models\Institution;
use App\Models\User;
use App\Models\Floor;
use App\Models\Hall;
use App\Models\GeneralInformation;
use App\Models\HallAudioName;
use App\Models\Language_Institution;

use Illuminate\Support\Facades\Storage;

class BasicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }//ak ovog nema onda postoji slobodan pristup stranici bez logiranja

    public function show_floors(){
       
        $inst_id = auth()->user()->institution_id;
        $floorsAll = Floor::all()->where('institution_id','==',$inst_id);
        $general_information_all = GeneralInformation::all();
        $general_information_show = new GeneralInformation;
        foreach($general_information_all as $gi_all){
            if($gi_all->institution_id == $inst_id){
                $general_information_show = $gi_all;
            }
        }
        
        return view('settings.basic', ['floors'=> $floorsAll, 'general_information'=>$general_information_show]);

    }

    // public function delete_floor($id){
       
    //     $floor = Floor::find($id);
    //     $floor->delete(); //ne brisat nego disable-at; ubaciti jos jedan red u tablicu ili napraviti neku novu tablicu koja
    //                         //sadrzi informacije o diablanim parametrima!
    //     return redirect()->back();

    //ruta : {{ route('delete_floor', ['floor'=> $floor->id]) }}
    // }

    public function store_floor(Request $request){

        $inst_id = auth()->user()->institution_id;//id od institucije kojoj pripada korisnik!

        $newFloor = new Floor;

        $flag = 0;
        if($request->floor == ""){ //ako se nista ne unese, ne ce se upisati u bazu!
            $flag = 1;
            echo '<script>alert("Unesite naziv kata!")</script>'; //baza je prazna alert->super
        }

        if($flag == 0){ //Unosi se jezik u language tablicu, a id je potrebno unjeti u tablicu language_institution
                        
            $newFloor->name = $request->floor;
            $newFloor->institution_id = $inst_id;
            $newFloor->save();
        }

       return redirect()->back();

    }

    public function edit_floor($id){

        $inst_id = auth()->user()->institution_id;
        $floor = Floor::find($id);
        $languages = Language_Institution::all()->where('institution_id', '==', $inst_id);
        return view('settings.editFloor',['floor'=> $floor, 'languages'=>$languages]);

    }

    public function store_hall(Request $request, $id){ //dodavanje sadrzaja za dvorane!!

        $inst_id = auth()->user()->institution_id;

        $languages = Language_Institution::all()->where('institution_id', '==', $inst_id);
        
        $validation = request()->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
            'audio.*' => 'mimes:audio/mpeg,mpga,mp3,wav,aac',
        ]);

        //u hall -> inst_id, floor_id, img, active je uvijek 1

        //hall audio name -> hall_id, language_id, audio, name

        if($request->hasfile('image')){

            $file_image = $request->file('image');
            $file_image_name = $file_image->getClientOriginalName();

            Storage::disk('sftp')->putFileAs("", $file_image, "./galerija-ruzic/slike/dvorane_novo/".$file_image_name);
           
        }else{
            $file_image_name = 'null';
        }

        $hall_new = new Hall;
        $hall_new->institution_id = $inst_id;
        $hall_new->floor_id = $id;
        $hall_new->image = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/dvorane_novo/".$file_image_name; //staviti sliku, gore odkomentirati!!
        $hall_new->save();


        $hall_id = $hall_new->id;

        $counter = 0;
        foreach($languages as $language){

            $hall_audio_name_new = new HallAudioName;

            $language_id = $language->language_id;

            //audio
            $file_audio = $request->file('audio')[$counter];
            $file_audio_name = $file_audio->getClientOriginalName();
            Storage::disk('sftp')->putFileAs("", $file_audio, "./galerija-ruzic/glazba/dvorane/".$file_audio_name);

            $name = $request->name[$counter];

            //store in hall_audio_name
            $hall_audio_name_new->hall_id = $hall_id;
            $hall_audio_name_new->language_id = $language_id;
            $hall_audio_name_new->audio = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/glazba/dvorane/".$file_audio_name;
            $hall_audio_name_new->name = $name;

            echo "hall_id: ".$hall_audio_name_new->hall_id." <br>";
            echo "language_id: ".$hall_audio_name_new->language_id." <br>";
            echo "audio: ".$hall_audio_name_new->audio." <br>";
            echo "name: ".$hall_audio_name_new->name." <br>";

            $hall_audio_name_new->save();
            echo $hall_audio_name_new."<br>";

            $counter++;
           
        }
        
        return redirect()->route('basic');
        
    }


}
