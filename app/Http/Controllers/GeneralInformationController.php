<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Models\GeneralInformation;

use Illuminate\Support\Facades\Storage;

class GeneralInformationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store_general_information(Request $request){ 

        $inst_id = auth()->user()->institution_id;
        $general_information_new = new GeneralInformation;
        $general_information = GeneralInformation::all()->where('institution_id', '==', $inst_id)->first();

        $flag = 0;

        $validation = request()->validate([
            
            'image' => 'mimes:jpeg,jpg,png,gif',
            'logo' => 'mimes:jpeg,jpg,png,gif',
            'audio' => 'mimes:audio/mpeg,mpga,mp3,wav,aac',
            
        ]);

        $general_information_new->institution_id = $inst_id;

        if($general_information == null){ 
            if($request->hasfile('image')){

                $file_image = $request->file('image');
                $file_image_name = $file_image->getClientOriginalName();
                Storage::disk('sftp')->putFileAs("", $file_image, "./galerija-ruzic/slike/general/".$file_image_name);
    
                $general_information_new->image = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/general/".$file_image_name;
               
            }else{
                $general_information_new->image = 'null';
            }
    
            if($request->hasfile('logo') ){
                
                $file_logo = $request->file('logo');
                $file_logo_name = $file_logo->getClientOriginalName();
                Storage::disk('sftp')->putFileAs("", $file_logo, "./galerija-ruzic/slike/general/".$file_logo_name);
    
                $general_information_new->logo = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/general/".$file_logo_name;
    
            }else{
                $general_information_new->logo = 'null';
            }
    
            if($request->hasfile('audio')){
    
                $file_audio = $request->file('audio');
                $file_audio_name = $file_audio->getClientOriginalName();               
                Storage::disk('sftp')->putFileAs("", $file_audio, "./galerija-ruzic/glazba/".$file_audio_name);
    
                $general_information_new->audio = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/glazba/".$file_audio_name;
    
            }else{
                $general_information_new->audio = 'null';
            }
    
            if($request->info_text != ""){
    
                $general_information_new->info_text = $request->info_text;
    
            }else{
    
                $general_information_new->info_text = " ";
    
            }
            $general_information_new->save();
        }
        else{
            
            if($request->hasfile('image')){

                $file_image = $request->file('image');
                $file_image_name = $file_image->getClientOriginalName();
                Storage::disk('sftp')->putFileAs("", $file_image, "./galerija-ruzic/slike/general/".$file_image_name);
    
                $general_information->image = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/general/".$file_image_name;
               
            }
    
            if($request->hasfile('logo') ){
                
                $file_logo = $request->file('logo');
                $file_logo_name = $file_logo->getClientOriginalName();
                Storage::disk('sftp')->putFileAs("", $file_logo, "./galerija-ruzic/slike/general/".$file_logo_name);
    
                $general_information->logo = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/general/".$file_logo_name;
    
            }
    
            if($request->hasfile('audio')){
    
                $file_audio = $request->file('audio');
                $file_audio_name = $file_audio->getClientOriginalName();       
                Storage::disk('sftp')->putFileAs("", $file_audio, "./galerija-ruzic/glazba/".$file_audio_name);
    
                $general_information->audio =  "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/glazba/".$file_audio_name;
    
            }
    
            if($request->info_text != ""){
    
                $general_information->info_text = $request->info_text;
    
            }else{
                $general_information->info_text = "";
            }  

            $general_information->save();
        }

        return redirect()->back();
        
    }
    
}
