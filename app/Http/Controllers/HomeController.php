<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Hall;
use App\Models\HallAudioName;
use App\Models\Floor;
use App\Models\Language;
use App\Models\Language_Institution;
use App\Models\Author_Institution;
use App\Models\Biography;
use App\Models\Art;
use App\Models\Author;
use App\Models\ArtName_Language;
use App\Models\GeneralInformation;
use App\Models\Localization;


use Illuminate\Support\Facades\Storage;

use Response; // za json file
use File;

class HomeController extends Controller
{    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $inst_id = auth()->user()->institution_id;
        $halls = Hall::all()->where('institution_id', '==', $inst_id);
        $hall_names = HallAudioName::all();
        return view('home', ['halls'=> $halls, 'hall_names'=> $hall_names]);
        
        
    }

    public function changeActivity(Request $request){

        $inst_id = auth()->user()->institution_id;
        $halls = Hall::all()->where('institution_id', '==', $inst_id);

        foreach($request->checkbox as $cb){
            foreach($halls as $hall){
                if($hall->id == $cb && $hall->active == 1){
                    $hall->active = 0;
                    $hall->save();
                }else if($hall->id == $cb && $hall->active == 0){
                    $hall->active = 1;
                    $hall->save();
                }
                
            }
            
        }
        
        return redirect()->back();
    }

    public function show(){
        $inst_id = auth()->user()->institution_id;
        // $users = User::all()->where('institution_id', '==', $inst_id)->sortBy('role');
        // return view('settings.users', ['users'=>$users]);
        $users_admin = User::all()->where('institution_id','==',$inst_id)->where('role','==', 1);
        $users_editor = User::all()->where('institution_id','==',$inst_id)->where('role','==', 2);
        return view('settings.users', ['users_admin'=>$users_admin, 'users_editor'=>$users_editor]);
    }

    public function show_begin_page(){
        return view('welcome');
    }

    // public function deleteUser($id){
    //     $user = User::find($id);
    //     $user->delete();
    //     return redirect()->back();

    //ruta : {{ route('delete_user', ['delete_user'=>$user->id]) }}
        
    // }

    // public function showEditBladeUser($id){

    //     $user = User::find($id);
    //     return view('settings.updateUser', ['user'=>$user]);
    

    //ruta : {{ route('show_edit_blade_user', ['update_user'=>$user->id]) }}
       
    // }

    // public function updateUser(){

        //ruta : {{ route('update_user', ['update_user'=>$user->id]) }}

    // }

    public function basic_function(){
        
        return view('settings.basic');
    }

    public function localization(){
        $inst_id = auth()->user()->institution_id;
        $languages = Language_Institution::all()->where("institution_id", $inst_id);
        return view('settings.localization', ['languages'=>$languages]);
    }

    public function store_localization(Request $request){
        $inst_id = auth()->user()->institution_id;
        $languages = Language_Institution::all()->where("institution_id", $inst_id);
        
        $counter = 0;
        foreach($languages as $language){
            $localization = new Localization;
            $localization->language_id = $language->language_id;
            $localization->institution_id = $inst_id;
            $localization->welcomeText = $request->welcomeText[$counter];
            $localization->btnEntryText = $request->entry[$counter];
            $localization->btnExitText = $request->exit[$counter];
            $localization->backToStart = $request->backToStart[$counter];
            $localization->next = $request->next[$counter];
            $localization->startTour = $request->startTour[$counter];
            $localization->atHall = $request->atHall[$counter];
            $localization->chooseHall = $request->chooseHall[$counter];
            $localization->save();

            $counter++;   
        }
       return redirect()->back();

    }

    public function jsonFile(){
        
        //za json umjetnika!!
        $inst_id = auth()->user()->institution_id;
        $authors = Author_Institution::all()->where('institution_id','==', $inst_id);
        $biographies = Biography::all()->where('institution_id','==', $inst_id);
        $arts = Art::all();

        $data = [

        ];

        foreach($authors as $author){
            $authorModel = Author::find($author->author_id);
            $authorData = [
                "artistId" => $authorModel->id,
                "artistName" => $authorModel->name,
                "biography" => [],
                "arts" => []
            ];

            foreach(Biography::where("author_id", $authorModel->id)->get() as $biography){
                $authorData["biography"][] = [
                    "language" => Language::where("id", $biography->language_id)->first()->short_language,
                    "bio" => $biography->biography
                ];
            }

            
            foreach(Art::where("author_id", $authorModel->id)->get() as $art){
                $details = [];
                foreach(ArtName_Language::where("art_id", $art->id)->get() as $art_id){
                    $details[] = [
                        "language" => $art_id->getLanguageRelation->short_language,
                        "artName" => $art_id->name,
                        "details" => $art->information
                    ];          
                }
                $authorData["arts"][] = [
                    "artId" => $art->id,
                    "hallNumber" => $art->hall_id, 
                    "details"=> $details,
                    "artUrl" => $art->image, 
                    "layout" => $art->layout
                ];
            }
            $data[] = $authorData;
        }
       
        
        Storage::disk('sftp')->put( "./galerija-ruzic/json_artist.json", json_encode($data));

        //za json sattings
        $general_information = GeneralInformation::where("institution_id", $inst_id)->get();
        //falit ce lokalizacija!!

        $store = [];
        foreach($general_information as $gi){
            $countFloors = 0;
            $info = [
                "clientID" => $gi->institution_id,
                "clientName" => $gi->getInstitutionRelation->name,
                "headerImg" => $gi->image,
                "logoImg" => $gi->logo,
                "musicURL" => $gi->audio,
                "localization" => [],
                "floorNum" => $countFloors,
                "floors" => [] 
            ];
            

            $localArray = [];
            foreach(Localization::where("institution_id", $inst_id)->get() as $localization){
                $local = [];
                $local["language"] = $localization->getLanguageRelation->short_language; 
                $local["welcomeText"] = $localization->welcomeText;
                $local["btnEntryText"] = $localization->btnEntryText;
                $local["btnExitText"] = $localization->btnExitText;
                $local["backToStart"] = $localization->backToStart;
                $local["next"] = $localization->next;
                $local["startTour"] = $localization->startTour;
                $local["atHall"] = $localization->atHall;
                $local["chooseHall"] = $localization->chooseHall;
                $counter = 0;
                foreach(Floor::where("institution_id", $inst_id)->get() as $floor){
                    $counter++;
                    $local["floor". $counter . "Name"] = $floor->name;
                }
                $localArray[] = $local;
            }
            $counter = 1;
            foreach(Floor::where("institution_id", $inst_id)->get() as $floor){
                $halls = [];

                
                $countHalls = 0;
                foreach(Hall::where("institution_id", $inst_id)->where("floor_id", $floor->id)->where("active", 1)->get() as $hall){
                    
                    $recordings = [];
                    foreach(HallAudioName::where("hall_id", $hall->id)->get()  as $hall_a_n){ //HallAudioName::where("hall_id", $hall->id)->get() 
                        
                        $recordings[] = [
                            "language" =>$hall_a_n->getLanguageRelation->short_language,
                            "hallName" => $hall_a_n->name,
                            "recordingURL" => $hall_a_n->audio
                        ];
                    }

                    $halls[] = [
                        "hallNum" => $hall->id,
                        "recordings" => $recordings
                    ];
                    $countHalls++;

                }
                $countFloors++;
                $info["floors"][] = [
                    "floorNum"=>$floor->id,
                    "hallsOnThisFloor" => $countHalls,
                    "halls" => $halls
                ];

                
    
            }
            $info["localization"] = $localArray;
            $info["floorNum"] = $countFloors;

            $store[] = $info; 
            // dd($info);

        }
        Storage::disk('sftp')->put( "./galerija-ruzic/json_settings.json", json_encode($store));
        // dd($store);

        return redirect()->route('home')->with('success', 'Sadržaj je uspješno objavljen!');   
        
        
    } 

}
