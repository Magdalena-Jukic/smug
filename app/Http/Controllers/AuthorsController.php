<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Language;
use App\Models\Author;
use App\Models\Biography;
use App\Models\Author_Institution;
use App\Models\Language_Institution;
use Intervention\Image\Facades\Image;

use Illuminate\Support\Facades\Storage;

class AuthorsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function showAuthors(){//prikaz svih umjetnika
        $inst_id = auth()->user()->getInstitutionRelation->id;

        $author_institution= Author_Institution::all()->where('institution_id','==', $inst_id);

        return view('authors.showAuthors', ['authors' => $author_institution]);
    }

    public function showLanguages(){//za biografiju koja se dodaje za svaki jezik posebno!
        $inst_id = auth()->user()->getInstitutionRelation->id;
        
        $language = Language_Institution::all()->where('institution_id','==',$inst_id);


        return view('authors.addAuthor', ['languages'=>$language]);
    }
    
    public function storeAuthor(Request $request){

        $inst_id = auth()->user()->getInstitutionRelation->id;
        //za dodavanje u tablicu Author
        $author = $request->name;
        
        $authorAdd = new Author;
        $authorAdd->name = $author;

        //provjera prije unošenja imena i prezimena
        $all = Author::all();
        $flag = 0;

        if($author == ""){ //ako se nista ne unese, ne ce se upisati u bazu!
            $flag = 1;
        }

        if($all != []){//ako nije prazno
            foreach($all as $a){
                if(strtolower($author) == strtolower($a->name)){
                    $flag = 1;
                    echo '<script>alert("Postoji u bazi");</script>';
                }
            }
        }

        if($flag == 0){
            $authorAdd->save();
        }else{
            echo "Autor već postoji u bazi!";
        }

        
        $author_id = $authorAdd->id;
        // = Author::all()->where('name', '==', $author)->first();
        

        //za dodavanje u tablicu Author_Institution
        $layout = $request->layout;
        
        $validation = request()->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        
        if($request->hasfile('image')){

            $file = $request->file('image');
            $filename = $file->getClientOriginalName(); 
            Storage::disk('sftp')->putFileAs("", $file, "./galerija-ruzic/slike/authors/".$filename);

        }else {
            $filename = "null";
        }
        //store author
        $authorInstitution = new Author_Institution;
        $authorInstitution->author_id = $author_id;
        $authorInstitution->institution_id = $inst_id;
        $authorInstitution->layout = $layout;
        $authorInstitution->image = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/authors/".$filename;

        $authorInstitution->save();

        $authorInstitution_id = $authorInstitution->id;
        $counter=0;
        foreach($request->language_id as $lan){ //ubacuje u tablicu smao one biografije koje su ispunjene!
            if($request->biography[$counter] == ""){
                echo "<br> Nema biografije";
            }else{
                $biography_save = new Biography;
                $biography_save->author_id = $authorInstitution_id;
                $biography_save->institution_id = $inst_id;
                $biography_save->language_id = $lan;
                $biography_save->biography = $request->biography[$counter];

                $biography_save->save();

                echo "<br>ezik:".$lan." biografija: " .$request->biography[$counter]; 
            }

            $counter++;
        }


      return redirect()->route('authors');
    }

    public function showEditBladeAuthor($id, $name){
        //id->AI name->A
        
        $inst_id = auth()->user()->getInstitutionRelation->id;
        $biographies = Biography::all()->where('institution_id','==', $inst_id)->where('author_id', '==', $id);
        $author = Author_Institution::where('author_id', $id)->get()->first();
        // dd($author);
        return view('authors.editAuthor', ['author'=>$author,'author_id'=>$id, 'author_name'=>$name, 'biographies'=>$biographies]);
    }

    public function editAuthor(Request $request, $author_id){ 
        
        $inst_id = auth()->user()->getInstitutionRelation->id;
        //izmjena slike
        $AI = Author_Institution::find($author_id);
        $layout = $request->layout;
        $AI->layout = $layout;
        $AI->save();

        $validation = request()->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        
        if($request->hasfile('image')){

            $file = $request->file('image');
            $filename = $file->getClientOriginalName(); // getting image extension
            Storage::disk('sftp')->putFileAs("", $file, "./galerija-ruzic/slike/authors/".$filename);
            $AI->image = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/authors/".$filename;
            $AI->save();

        }else {
            $filename = Author_Institution::find($author_id)->image;
        }
        
        $all = Biography::all()->where('institution_id', '==', $inst_id)
                                ->where('author_id', '==', $author_id);
             
        $i=1;
        $j=1;
        foreach($all as $bio){
            echo "id iz baze: ".$i." <br>";
            
            foreach($request->biography as $entered_bio){
                echo "id mog tog: ".$j." <br>";
                if($i == $j && $bio->biography == $entered_bio){
                    echo "polja se poklapaju <br>";
                    echo $bio->biography ."=". $entered_bio."<br>";
                }elseif($i == $j && $bio->biography != $entered_bio){
                    $bio->biography = $entered_bio;
                    $bio->save();
                    echo "projmena koja se sprema <br>";
                    echo $bio->biography ."->". $entered_bio."<br>";
                }
                else{
                    echo "razlicite su <br>";
                }
                $j++;
            }
            $j=1;
            $i++;
           
        }     
        
        return redirect()->back();
        
    }
    
}
