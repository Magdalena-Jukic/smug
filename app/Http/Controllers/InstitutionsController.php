<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Institution;
use App\Models\User;

class InstitutionsController extends Controller
{
    public function index(){
        return view('auth.institution');
       
    }

    public function store(Request $request){ // ne treba biti prijavljen da se unese nova institucij!
              
        $name = $request->name;
        $address = $request->address;
        $postal_number = $request->postal_number;
        $city = $request->city;
        $email = $request->email;
        $phone = $request->phone;

        $inst = new Institution;
        $inst->name = $name;
        $inst->address = $address;
        $inst->postal_number = $postal_number;
        $inst->city = $city;
        $inst->email = $email;
        $inst->phone = $phone;
    
        $inst->save();
        return redirect()->route('register');

    }

}
