<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Floor;
use App\Models\Hall;
//ispočetka napraviti FloorController
class FloorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }//ak ovog nema onda postoji slobodan pristup stranici bez logiranja


    public function showaddFloors(){
        $inst_id = auth()->user()->institution_id;
        $floorsAll = Floor::all()->where('institution_id','==',$inst_id);

        return view('content.addFloor', ['floors'=> $floorsAll]);
    }

    public function storeFloor(){
        echo "store function";
    }



    /*public function storeFloor(Request $request){
        $inst_id = auth()->user()->getInstitutionRelation->id;
        $floor = $request->floor;
        $hall = $request->hall;

        //echo "kat: ". $floor. " a dvorana: ". $hall. " id institutcije: ". $inst_id;

        //provjera postoji li taj kat već unesen!
        $floorLow = strtolower($floor);
        $floorAll = Floor::all()->where('institution_id', '==', $inst_id);

        $flagFloor = 0;

        if($floorLow == ""){ //ako se nista ne unese, ne ce se upisati u bazu!
            $flagFloor = 1;
        }

        if($floorAll != []){
            foreach($floorAll as $fa){
                if(strtolower($fa->name) == $floorLow){
                    $flagFloor = 1;
                }
            }
        }

        //kraj provjere

        if($flagFloor == 0){
            $floorNew = new Floor;
            $floorNew->name = $floorLow;
            $floorNew->institution_id = $inst_id;
            $floorNew->save();
            echo "Kat spremljen!";
        }else{
            echo "Kat već postoji u bazi!";
        }

        //spremanje dvorana u bazu
        //inst_id
        //floor_id
        //name

        $hallLow = strtolower($hall);
        $hallAll = Hall::all()->where('institution_id', '==', $inst_id);
        foreach($floorAll as $fa){
            if(strtolower($fa->name) == $floorLow){
                $floor_id = $fa->id;
            }
        }
        //echo "id kata za koji se unosi dvorana je ".$floor_id;
        
        $flagHall = 0;

        if($hallLow == ""){ //ako se nista ne unese, ne ce se upisati u bazu!
            $flagHall = 1;
        }

        if($hallAll != []){
            foreach($floorAll as $floor){//kod dvorane se mogu ponoviti nazivi, ali ne smije ponoviti floor_id i inst_id
                foreach($hallAll as $hall){
                    if($floor->id == $hall->floor_id){
                        foreach($hall as $h){
                            if($hall->name == $hallLow){
                                
                            }
                        }
                        $flagHall = 1;
                    } 
                }
            }
        }

        //kraj provjere

        if($flagHall == 0){
            $hallNew = new Hall;
            $hallNew->name = $hallLow;
            $hallNew->institution_id = $inst_id;
            $hallNew->floor_id = $floor_id->id;
            $hallNew->save();
            echo "Dvorana spremljen!";
        }else{
            echo "Dvorana već postoji u bazi!";
        }
        

        //return redirect('/content/addFloor');

        
    }*/

    public function new(Request $request){
        $inst_id = auth()->user()->getInstitutionRelation->id;
        $floorHall = Floor::all()->where('institution_id', '==', $inst_id);
        $hallAll = Hall::all()->where('institution_id', '==', $inst_id); 

    }
}
