<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Language;
use App\Models\Institution;
use App\Models\User;
use App\Models\Biography;
use App\Models\Language_Institution;

class LanguagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request){ 
        $inst_id = auth()->user()->institution_id;

        $languageAll = Language::all();
        $language_institution = new Language_Institution;
        // $language_institution->languagege_id = 2;
        // $language_institution->institution_id = $inst_id;
        // dd($language_institution);
        //echo '<script>alert("baza je prazna")</script>'; //baza je prazna alert->super

        $flag = 0;
        if($request->languages == ""){ //ako se nista ne unese, ne ce se upisati u bazu!
            $flag = 1;
        }

        if($flag == 0){ //Unosi se jezik u language tablicu, a id je potrebno unjeti u tablicu language_institution

            foreach($languageAll as $lan){ //ako nije prazno polje za upis jezika 
                // dd($lan->language);
                if(strtolower($lan->language. " jezik") == strtolower($request->languages)){
                    
                    $language_institution->language_id = $lan->id;
                    $language_institution->institution_id = $inst_id;
                    $language_institution->save();
                    break;
                }
                
            }
        }else{
            echo "Jezik veĆ postoji u bazi!";
        }
        
        
       return redirect()->back();

    }

    public function show(){
        $inst_id = auth()->user()->institution_id;
        
        $language_inst = Language_Institution::all()->where('institution_id','==',$inst_id);

        $languageAll = Language::all();
        
       return view('settings.language', ['language'=>$languageAll, 'language_inst'=>$language_inst]);

    }
    
    public function delete($id){ //radi, trazi samo po id-ju i brise!
        $language = Language_Institution::find($id);
        $language->delete();//briše red u kojem se nalazi institucija i jezik
        
        return redirect()->back();
        
    }

}
