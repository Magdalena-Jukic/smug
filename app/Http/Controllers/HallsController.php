<?php

namespace App\Http\Controllers;
use Endroid\QrCode\QrCode;

use Illuminate\Http\Request;
use App\Models\Floor;
use App\Models\Hall;
use App\Models\Language_Institution;
use App\Models\Author_Institution;
use App\Models\Author;
use App\Models\Art;
use App\Models\ArtName_Language;
use App\Models\Language;
use App\Models\Biography;
use App\Models\HallAudioName;

use Illuminate\Support\Facades\Storage;

class HallsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function showArt($floor_id, $hall_id){
        
        $floor = Floor::find($floor_id); //find gleda bas stupac id
        $hall = HallAudioName::all()->where('hall_id','==', $hall_id)->where('language_id','==', '9'); //za sad je ovako sto se jezika tice

        $arts = Art::all()->where("hall_id", $hall_id);     
        
        return view('content.showArt', ['floor'=>$floor, 'hall'=>$hall, 'arts' => $arts, 'hall_id'=>$hall_id]);
        
    }

    public function showEditBladeArt($id){

        $art = Art::find($id);
        $art_name = ArtName_Language::all()->where('art_id','==', $art->id);

        return view('content.editArt', ['art'=> $art, 'art_name'=>$art_name]);
        // $biographies = Biography::all()->where('institution_id','==', $inst_id)->where('author_id', '==', $id);

        // return view('authors.editAuthor', ['author_id'=>$id, 'author_name'=>$name, 'biographies'=>$biographies]);
    }

  
    public function show_addArt($floor_id, $hall_id){

        $inst_id = auth()->user()->getInstitutionRelation->id;
        $floor = Floor::find($floor_id);
        $hall = Hall::find($hall_id); 
        // dd($hall);
        //$hall = HallAudioName::all()->where('hall_id','==', $hall_id)->where('language_id','==', '1'); //za sad je ovako sto se jezika tice

        $languages = Language_Institution::all()->where('institution_id','==', $inst_id);
        $author_institution = Author_Institution::all()->where('institution_id','==',$inst_id);

        
        $hall_name = HallAudioName::all()->where('hall_id', '==', $hall_id)->where('language_id','==', '1');
        
        
        
        return view('content.addArt', ['authors'=>$author_institution, 'floor'=>$floor, 'hall'=>$hall, 'languages' => $languages, 'hall_name'=>$hall_name]);
    }

    public function storeArt(Request $request, $floor_id, $hall_id){
        
        //spremanje umjetnine u bazu: author_id, institution_id
        // informatino, layout, image!
        $inst_id = auth()->user()->getInstitutionRelation->id;
        $artistsAll = Author_Institution::all()->where('institution_id','==', $inst_id);
        
        foreach($artistsAll as $aAll){
            if($aAll->getAuthorsRelation->name == $request->name){
                $author_id = $aAll->author_id;
                break;
            }else{
                echo '<script type="text/javascript">alert("Nema autora u instituciji.")</script>';
            }
        }

        if($request->hasfile('image')){
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            Storage::disk('sftp')->putFileAs("", $file, "./galerija-ruzic/slike/arts/".$filename);
        }else {
            $filename = "null";
        }

        $layout = $request->layout;
        $information = $request->info;

        $art = new Art;
        $art->author_id = $author_id;
        $art->hall_id = $hall_id;
        $art->image = "http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/arts/".$filename;
        $art->layout = $layout;
        $art->information = $information;

        echo "autor_id: ". $art->author_id." inst_id: ".$art->hall_id." slika: ".$art->image. " layout: ".$art->layout." info: ".$art->information;
        $art->save();


        //spremanje naziva
        $art_id = $art->id;
        echo "id tek unesenog:". $art_id;


        $counter=0;
        foreach($request->language_id as $lan){ //ubacuje u tablicu smao one biografije koje su ispunjene!
            if($request->artName[$counter] == ""){
                echo "<br> Nema imena";
            }else{
                $art_name_save = new ArtName_Language;
                $art_name_save->art_id = $art_id;
                $art_name_save->language_id = $lan;
                $art_name_save->name = $request->artName[$counter];

                $art_name_save->save();

            }

            $counter++;
        }
        
        return redirect()->route('showArt', ['floor'=> $floor_id, 'hall'=>$hall_id]);

    }
    public function editArt(Request $request, $art_id){ //izmjene informacija o umjetniku

        /*$inst_id = auth()->user()->getInstitutionRelation->id;
        
        $art = Art::find($art_id);
        $layout = $request->layout;
        $art->layout = $layout;
        $art->save();

        $validation = request()->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        
        if($request->hasfile('image')){

            $file = $request->file('image');
            $filename = $file->getClientOriginalName(); // getting image extension
            Storage::disk('sftp')->putFileAs("", $file, "./galerija-ruzic/slike/arts/".$filename);
            $art->image ="http://simpert.hr.dedivirt2097.your-server.de/unity/galerija-ruzic/slike/arts/".$filename;
            $art->save();

        }else {
            $filename = $art->image;
        }*/
        return redirect()->back();
       

    }

    
}
