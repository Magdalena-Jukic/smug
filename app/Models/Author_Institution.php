<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author_Institution extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'image',
        'layout',
    ];

    public function getAuthorsRelation(){

        return $this->belongsTo(Author::class, 'author_id', 'id');
    }

    public function getInstitutionRelation(){

        return $this->belongsTo(Institution::class, 'institution_id');
    }
}
