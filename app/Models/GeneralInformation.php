<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneralInformation extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'image', 
        'logo',
        'info_text',
        'audio',
        //iz tablice dodat
        /*
        $table->bigInteger('institution_id');
            $table->string('image')->nullable();
            $table->string('logo')->nullable();
            $table->string('info_text');
            $table->string('audio')->nullable();
        */
    ];

    public function getInstitutionRelation(){
        return $this->belongsTo(Institution::class, 'institution_id', 'id');
    }
}
