<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Localization extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'welcomeText',
        'btnEntryText',
        'btnExitText',
        'backToStart',
        'next',
        'startTour',
        'atHall',
        'chooseHall'
        
    ];
    //napraviti novi model Localization, sa svim atributima \ {"floor1Name", "floor2Name", "hall","language"} 
        //dodatni atributi language_id i institution_id
        /*
        "localization" : [
            {
                "language"     : "hr",
                "welcomeText"  : "Dobro došli u Galeriju Ružić",
                "btnEntryText" : "ULAZ",
                "btnExitText"  : "IZLAZ",
                "floor1Name"   : "Prizemlje",
                "floor2Name"   : "Prvi kat",
                "backToStart"  : "Povratak na početni zaslon",
                "next"         : "NAPRIJED",
                "hall"         : "Dvorana",
                "startTour"    : "Kreni u obilazak",
                "atHall"       : "Trenutno ste na lokaciji",
                "chooseHall"   : "U koju dvoranu želite ući?"
            },
            {
                "language"     : "en",
                "welcomeText"  : "Welcome to Ružić Gallery",
                "btnEntryText" : "ENTER",
                "btnExitText"  : "EXIT",
                "floor1Name"   : "First Floor",
                "floor2Name"   : "Second Floor",
                "backToStart"  : "Return to start screen",
                "next"         : "NEXT",
                "hall"         : "Hall",
                "startTour"    : "Start the tour",
                "atHall"       : "You are currently at",
                "chooseHall"   : "Which hall do you want to enter?"
            }
        ],
        */

        public function getLanguageRelation(){

            return $this->belongsTo(Language::class, 'language_id', 'id');
        }
}
