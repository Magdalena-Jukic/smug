<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = [
        'name',
        'address',
        'postal_number',
        'city',
        'email',
        'phone',
    ];
    
    public function getUserRelation(){

        return $this->hasMany(User::class, 'institution_id', 'id');//user tablica mora imati institution_id stupac!
    }

    /*public function getLanguageRelation(){

        return $this->hasMany(Language::class, 'institution_id', 'id');//language tablica mora imati institution_id stupac!
    }*/

    public function getFloorRelation(){

        return $this->hasMany(Floor::class, 'institution_id', 'id');//Floor tablica mora imati institution_id stupac!
    }
    public function getHallRelation(){

        return $this->hasMany(Hall::class, 'institution_id', 'id');//Floor tablica mora imati institution_id stupac!
    }
}
