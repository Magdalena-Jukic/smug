<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArtName_Language extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'name',
    ];

    public function getArtRelation(){

        return $this->belongsTo(Art::class, 'art_id', 'id');
    }
    public function getLanguageRelation(){

        return $this->belongsTo(Language_Institution::class, 'language_id', 'id');
    }
}
