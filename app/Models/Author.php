<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'name',
    ];

    public function getAuthorInstitutionRelation(){

        return $this->belongsToMany('Institution', 'Author_Institution', 'author_id', 'institution_id');
    }
}
