<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Art extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'image',
        'layout',
        'information',
    ];

    public function getAuthorRelation(){
        
        return $this->belongsTo(Author::class, 'author_id', 'id');
    }

    public function getArtNameLanguageRelation(){
        return $this->hasMany(ArtName_Language::class, 'art_id', 'id');
    }

   
}
