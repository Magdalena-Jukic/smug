<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'name',
    ];

    public function getInstitutionRelation(){

        return $this->belongsTo(Institution::class, 'institution_id', 'id');
    }

    public function getHallRelation(){
        
        return $this->hasMany(Hall::class, 'floor_id', 'id');
    }
}
