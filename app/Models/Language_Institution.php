<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language_Institution extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = [
        'language_id', 
        'institution_id'
    ];

    public function getInstitutionRelation(){

        return $this->belongsTo(Institution::class, 'institution_id', 'id');
    }
    

    public function getLanguageRelation(){
        
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }

}
