<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HallAudioName extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'audio',
        'name',
        
    ];

    public function getHallRelation(){

        return $this->belongsTo(Hall::class, 'hall_id', 'id');
    }

    public function getLanguageRelation(){

        return $this->belongsTo(Language::class, 'language_id', 'id');
    }
}
