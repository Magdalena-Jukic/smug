<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'image',
        'active',
    ];

    public function getFloorRelation(){

        return $this->belongsTo(Floor::class, 'floor_id', 'id');
    }

    public function getInstitutionRelation(){

        return $this->belongsTo(Institution::class, 'institution_id', 'id');
    }

    public function getAudioNameRelation(){

        return $this->hasMany(HallAudioName::class, 'hall_id', 'id');
    }

    public function getArtRelation(){
        return $this->hasMany(Art::class);
    }
}
