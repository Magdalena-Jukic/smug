<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Biography extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'biography',
    ];

    public function getLanguageRelation(){
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }

    public function getAuthorRelation(){
        return $this->belongsTo(Author_Institution::class, 'author_id', 'id');
    }

}

