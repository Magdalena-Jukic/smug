<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Floor;
use App\Models\HallAudioName;
use App\Models\Hall;
use App\Models\Institution;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('sidebar.sidebar', function($view){
            $view->with('floors', Floor::orderBy("id")->get()); //Floor::orderBy("id", "desc")
        });

        view()->composer('sidebar.sidebar', function($view){
            $view->with('halls', HallAudioName::orderBy("id")->get()); //Hall::orderBy("id", "desc")
        });

        view()->composer('sidebar.sidebar', function($view){
            $inst_id = auth()->user()->institution_id;
            $view->with('institution', Institution::find($inst_id)); //Hall::orderBy("id", "desc")
        });
    }
}
